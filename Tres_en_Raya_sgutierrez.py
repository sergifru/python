# Tres en raya

import random

def drawBoard(board):


    # "board" es un tablero con 9 numeros en total que lo representan.
    print('   |   |')
    print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9])
    print('   |   |')
    print('___________')
    print('   |   |')
    print(' ' + board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('   |   |')
    print('___________')
    print('   |   |')
    print(' ' + board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('   |   |')

def inputPlayerLetter():

    letter = ''
    while not (letter == 'X' or letter == 'O'):
        print('Cual te gustaria elegir entre X o O?')
        letter = input().upper()

    # numero de la máquina.
    if letter == 'X':
        return ['X', 'O']
    else:
        return ['O', 'X']

def whoGoesFirst():

    if random.randint(0, 1) == 0:
        return 'computer'
    else:
        return 'player'

def playAgain():
    #   Si el jugador quiere volver a jugar de nuevo.
    print('Quieres jugar de nuevo? (si o no)')
    return input().lower().startswith('si')

def makeMove(board, letter, move):
    board[move] = letter

def isWinner(bo, le):
    # crear un nuevo tablero si el jugador decide jugar de nuevo.
    # si el jugador gana, nuevo tablero y elegir X o O de nuevo.

    return ((bo[7] == le and bo[8] == le and bo[9] == le) or # por arriba
    (bo[4] == le and bo[5] == le and bo[6] == le) or # por el medio
    (bo[1] == le and bo[2] == le and bo[3] == le) or # por abajo
    (bo[7] == le and bo[4] == le and bo[1] == le) or # abajo en el lado izquierdo
    (bo[8] == le and bo[5] == le and bo[2] == le) or # abajo al medio
    (bo[9] == le and bo[6] == le and bo[3] == le) or # abajo en el lado derecho
    (bo[7] == le and bo[5] == le and bo[3] == le) or # diagonal
    (bo[9] == le and bo[5] == le and bo[1] == le)) # diagonal

def getBoardCopy(board):
    # Duplicar el cuadro y volverlo a duplicar.
    dupeBoard = []

    for i in board:
        dupeBoard.append(i)

    return dupeBoard

def isSpaceFree(board, move):
    # Vuelve True si el espacio esta vacio
    return board[move] == ' '

def getPlayerMove(board):
    # Movimiento del jugador.
    move = ' '
    while move not in '1 2 3 4 5 6 7 8 9'.split() or not isSpaceFree(board, int(move)):
        print('Cual es tu siguiente movimiento? (1-9)')
        move = input()
    return int(move)

def chooseRandomMoveFromList(board, movesList):
    # Vuelve a un movimiento valido del pasado tablero.
    # Vuelve None si no es un movimiento valido.
    possibleMoves = []
    for i in movesList:
        if isSpaceFree(board, i):
            possibleMoves.append(i)

    if len(possibleMoves) != 0:
        return random.choice(possibleMoves)
    else:
        return None

def getComputerMove(board, computerLetter):
    # Letra de la maquina

    if computerLetter == 'X':
        playerLetter = 'O'
    else:
        playerLetter = 'X'

    # Tres en raya IA:
    # Primero la máquina se fija en si puede ganar en el siguiente movimiento.
    for i in range(1, 10):
        copy = getBoardCopy(board)
        if isSpaceFree(copy, i):
            makeMove(copy, computerLetter, i)
            if isWinner(copy, computerLetter):
                return i

    # Observa si el jugador gana en la siguiente ronda, si es asi bloquealo.
    for i in range(1, 10):
        copy = getBoardCopy(board)
        if isSpaceFree(copy, i):
            makeMove(copy, playerLetter, i)
            if isWinner(copy, playerLetter):
                return i

    # Intenta escoger una de estas esquinas si están libres.
    move = chooseRandomMoveFromList(board, [1, 3, 7, 9])
    if move != None:
        return move

    # Intenta escoger el centro si está vacio.
    if isSpaceFree(board, 5):
        return 5

    # Muevete por una de estas casillas.
    return chooseRandomMoveFromList(board, [2, 4, 6, 8])

def isBoardFull(board):
    # vuelve True si el tablero esta lleno.
    # vuelve False.
    for i in range(1, 10):
        if isSpaceFree(board, i):
            return False
    return True


print('Bienvenido al Tres en Raya!')

while True:
    # Reiniciar el Tablero
    theBoard = [' '] * 10
    playerLetter, computerLetter = inputPlayerLetter()
    turn = whoGoesFirst()
    print('El ' + turn + ' va primero.')
    gameIsPlaying = True

    while gameIsPlaying:
        if turn == 'player':
            # Turno del jugador.
            drawBoard(theBoard)
            move = getPlayerMove(theBoard)
            makeMove(theBoard, playerLetter, move)

            if isWinner(theBoard, playerLetter):
                drawBoard(theBoard)
                print('Bien hecho! Ganaste la partida!')
                gameIsPlaying = False
            else:
                if isBoardFull(theBoard):
                    drawBoard(theBoard)
                    print('Empate, el tablero está completo!')
                    break
                else:
                    turn = 'computer'

        else:
            # Turno de la máquina
            move = getComputerMove(theBoard, computerLetter)
            makeMove(theBoard, computerLetter, move)

            if isWinner(theBoard, computerLetter):
                drawBoard(theBoard)
                print('No fuiste capaz de ganar a una máquina! Perdiste, intentalo la próxima vez.')
                gameIsPlaying = False
            else:
                if isBoardFull(theBoard):
                    drawBoard(theBoard)
                    print('Empate, el tablero está completo!')
                    break
                else:
                    turn = 'player'

    if not playAgain():
        break
