# Proytecto Tkinter sgutierrez

import tkinter
import random

# posibles colores
colores = ['Red', 'Blue', 'Green', 'Pink', 'Black', 'Yellow', 'Orange', 'White', 'Purple', 'Brown']
# el puntuaje comienza en cero
puntuaje = 0
# la duracion del juego
tiempo = 30


# inicia el juego
def iniciarjuego(event):
    if tiempo == 30:
        # cuenta regresiva
        cuentaregresiva()

    # proxima funcion
    siguientecolor()


# escoge y muestra el siguiente color
def siguientecolor():
    # se utilizan las variables iniciales
    global puntuaje
    global tiempo

    # si el juego esta en funcionamiento...
    if tiempo > 0:
        # ...reactiva la caja de texto
        entrada.focus_set()
        # si el color escrito es igual al del texto...
        if entrada.get().lower() == colores[1].lower():
            # ...se suma un punto
            puntuaje += 1

        # despues de ingresar la respuesta, se borra el texto en la caja
        entrada.delete(0, tkinter.END)
        # nuevos colores
        random.shuffle(colores)
        # se muestra el nombre del color con texto de distinto color
        etiqcolor.config(fg=str(colores[1]), text=str(colores[0]))
        # actualiza la puntuacion
        etiqpuntuaje.config(text="Puntuaje: " + str(puntuaje))


# funcion que realiza una cuenta regresiva
def cuentaregresiva():
    global tiempo
    # si el juego esta en funcionamiento...
    if tiempo > 0:
        # se disminuye el tiempo
        tiempo -= 1
        # se va actualizando el tiempo restante
        etiqtiempo.config(text="Tiempo restante: " + str(tiempo))
        # reinicia la funcion
        etiqtiempo.after(1000, cuentaregresiva)


# VENTANA TK
ventana = tkinter.Tk()
ventana.title("Juego de Colores")
ventana.geometry("600x300")
ventana.config(bg='#ccffcc')

# ETIQUETA DE INSTRUCCIONES
instrucciones = tkinter.Label(ventana, text="Escribe el color en ingles de la palabra, no el texto que aparece!",font=('Lobster', 12, "bold",),background="#ccffcc")
instrucciones.pack()

# ETIQUETA DE PUNTUAJE
etiqpuntuaje = tkinter.Label(ventana, text="Presiona Enter para comenzar", font=('Lobster', 12,"bold"),background="#ccffcc")
etiqpuntuaje.pack()

# ETIQUETA DE TIEMPO
etiqtiempo = tkinter.Label(ventana, text="Tiempo restante: " + str(tiempo), font=('Lobster', 12,"bold"),background="#ccffcc")
etiqtiempo.pack()

# ETIQUETA QUE MUESTRA LOS COLORES
etiqcolor = tkinter.Label(ventana, font=('Lobster', 60,"bold"),background="#ccffcc")
etiqcolor.pack()

# ENTRADA PARA LA RESPUESTA
entrada = tkinter.Entry(ventana)
# comenzar el juego cuando se la click a enter
ventana.bind('<Return>', iniciarjuego)
entrada.pack()
# enfocarse en la caja de texto
entrada.focus_set()

# iniciar el programa
ventana.mainloop()